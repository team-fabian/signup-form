import "./App.css";
import Navbar from "./component/Navbar/Navbar";
import SignUpTab from "./component/Signuptab/SignUpTab";
import React, { Component } from "react";
class App extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <SignUpTab />
      </div>
    );
  }
}

export default App;
