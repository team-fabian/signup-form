import React, { Component } from "react";

class OnSubmitTab extends Component {
  render() {
    return (
      <div>
        <h1 className="text-primary mt-3 mh-100  d-flex mx-auto justify-content-center mt-4">
          <i class="fa-regular fa-face-smile"></i>
          You have Sucessfully Signed up with SocialSphere !!
        </h1>
      </div>
    );
  }
}

export default OnSubmitTab;
