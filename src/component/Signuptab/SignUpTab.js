import React, { Component } from "react";
import validator from "validator";
import OnSubmitTab from "../OnSubmitTab/OnSubmitTab";

export default class SignUpTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      FirstName: "",
      LastName: "",
      Age: "",
      Gender: "",
      Role: "",
      Email: "",
      password: "",
      confirmPassword: "",
      isChecked: false,
      passed: false,
      errors: {},
    };
  }

  handleInputChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const {
      FirstName,
      Age,
      Email,
      password,
      LastName,
      confirmPassword,
      isChecked,
      passed,
    } = this.state;
    const errors = {};

    // Validate FirstName & LastName
    if (validator.isEmpty(FirstName)) {
      errors.FirstName = "First Name Field can't be blank";
    } else if (!validator.isAlpha(FirstName)) {
      errors.FirstName = "First Name must contain only alphabets";
    }

    if (validator.isEmpty(LastName)) {
      errors.LastName = "Last Name Field can't be blank";
    } else if (!validator.isAlpha(LastName)) {
      errors.LastName = "Last Name must contain only alphabets";
    }

    if (!validator.isInt(Age)) {
      errors.Age = "Enter Valid Number";
    } else if (Age < 0 || Age > 100) {
      errors.Age = "Please Enter Age between 0 - 100 ";
    }

    if (!validator.isEmail(Email)) {
      errors.Email = "Invalid Email address";
    }

    if (!validator.isLength(password, { min: 6 })) {
      errors.password = "Password must be at least 6 characters long.";
    }

    if (password !== confirmPassword) {
      errors.confirmPassword = "Password does not match.";
    }
    // If there are errors, set the state and return
    if (Object.keys(errors).length > 0) {
      this.setState({ errors });
      return;
    }

    // Form submission logic
    console.log("Form submitted!");
    this.setState({ passed: true });
  };
  render() {
    const {
      FirstName,
      LastName,
      password,
      errors,
      Gender,
      Age,
      Role,
      Email,
      confirmPassword,
      isChecked,
      passed,
    } = this.state;
    return (
      <>
        {passed && <OnSubmitTab />}

        {!passed && (
          <div className=" mt-3 mh-100  d-flex mx-auto justify-content-center ">
            <form
              className="shadow border p-4 bg-light"
              onSubmit={this.handleSubmit}
            >
              <h1 className="font-weight-bold">Sign up</h1>
              <div className="row mt-4">
                <div className="col">
                  <label className="font-weight-bold h5" name="FirstName">
                    First Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Enter First name"
                    name="FirstName"
                    value={FirstName}
                    onChange={this.handleInputChange}
                  />
                  {errors.FirstName && (
                    <p className="text-danger">{errors.FirstName}</p>
                  )}
                </div>
                <div className="col">
                  <label className="font-weight-bold h5" name="Last Name">
                    Last Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Enter Last Name"
                    name="LastName"
                    value={LastName}
                    onChange={this.handleInputChange}
                  />
                  {errors.LastName && (
                    <p className="text-danger">{errors.LastName}</p>
                  )}
                </div>
              </div>
              <div className="row mt-2">
                <div className="col">
                  <label className="font-weight-bold h6" labelname="Age">
                    Age:
                  </label>
                  <input
                    type="text"
                    className="form-control w-25"
                    placeholder="Enter age"
                    name="Age"
                    value={Age}
                    onChange={this.handleInputChange}
                  />
                  {errors.Age && <p className="text-danger">{errors.Age}</p>}
                </div>
              </div>
              <div className="row mt-2 w-50 mx-1">
                <label className="font-weight-bold h6" name="Gender">
                  Gender:
                </label>
                <select
                  className="form-control"
                  id="gender"
                  name="Gender"
                  value={Gender}
                  onChange={this.handleInputChange}
                  required
                >
                  <option value="">Select Gender</option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                  <option value="Others">Others</option>
                </select>
              </div>

              <div className="row mt-2 w-50 mx-1">
                <label className="font-weight-bold h6" name="Role">
                  Role
                </label>
                <select
                  className="form-control"
                  id="role"
                  name="Role"
                  value={Role}
                  onChange={this.handleInputChange}
                  required
                >
                  <option value="">Select Role</option>
                  <option value="Developer">Developer</option>
                  <option value="Senior Developer">Senior Developer</option>
                  <option value="Lead Engineer">Lead Engineer</option>
                  <option value="Lead CTO">Lead CTO</option>
                </select>
              </div>

              <div className="form-group row mt-2 mx-1 d-flex flex-column">
                <div className="">
                  {" "}
                  <label for="inputEmail3" className=" sm-10 font-weight-bold">
                    Email:
                  </label>
                </div>

                <input
                  type="email"
                  className="form-control w-75"
                  id="inputEmail3"
                  placeholder="Enter Email address"
                  name="Email"
                  value={Email}
                  onChange={this.handleInputChange}
                />
                {errors.Email && <p className="text-danger">{errors.Email}</p>}
              </div>
              <div className="form-group row mt-2">
                <label
                  forhtml="inputPassword1"
                  className="col-sm-5 col-form-label font-weight-bold"
                >
                  Password
                </label>
                <div className="col-sm-10">
                  <input
                    type="password"
                    className="form-control"
                    id="inputPassword3"
                    placeholder="Password"
                    name="password"
                    value={password}
                    onChange={this.handleInputChange}
                    required
                  />
                  {errors.password && (
                    <p className="text-danger">{errors.password}</p>
                  )}
                </div>
              </div>
              <div className="form-group row mt-2">
                <label
                  forhtml="Confirm Password"
                  className="col-sm-5 col-form-label font-weight-bold"
                >
                  Confirm Password
                </label>
                <div className="col-sm-10">
                  <input
                    type="password"
                    className="form-control"
                    id="inputPassword2"
                    placeholder="Confirm Password"
                    name="confirmPassword"
                    value={confirmPassword}
                    onChange={this.handleInputChange}
                    required
                  />
                  {errors.confirmPassword && (
                    <p className="text-danger">{errors.confirmPassword}</p>
                  )}
                </div>
              </div>
              <div className="form-group row mt-2">
                <div className="col-sm-10">
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      id="gridCheck1"
                      name="isChecked"
                      value={true}
                      onChange={this.handleInputChange}
                      required
                    />
                    <label
                      className="form-check-label font-weight-bold"
                      forhtml="gridCheck1"
                    >
                      Agree terms and conditions
                    </label>
                  </div>
                </div>
              </div>
              <button className="btn btn-primary" type="submit">
                Submit
              </button>
            </form>
          </div>
        )}
      </>
    );
  }
}
