import React, { Component } from "react";

export default class Navbar extends Component {
  render() {
    return (
      <div className="navbar" style={{ height: "5rem" }}>
        <nav className="navbar navbar-dark bg-primary h-100 d-inline-block container-fluid d-flex  justify-content-center text-white">
          <i class="fa-solid fa-hashtag fa-2x"></i>
          <h1 className="font-weight-bold"> SocialSphere</h1>
        </nav>
      </div>
    );
  }
}
